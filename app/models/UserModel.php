<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class UserModel extends Eloquent {
	protected $fillable = array('username', 'password', 'otoritas');
	protected $table      = 'data_user';
	protected $hidden     = array('password');
	public $timestamps = false;
}