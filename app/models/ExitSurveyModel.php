<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class ExitSurveyModel extends Eloquent {
	protected $fillable = array('*');
	protected $table    = 'exit_survey';	
	public $timestamps  = false;
}